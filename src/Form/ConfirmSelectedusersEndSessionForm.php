<?php

namespace Drupal\eus\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmSelectedusersEndSessionForm.
 */
class ConfirmSelectedusersEndSessionForm extends ConfirmFormBase {

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Drupal\Core\Session\SessionManagerInterface definition.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Use core services object.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('session_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "confirm_eus_all_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to destroy session of these user accounts?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Destroy session');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $accounts = $this->tempStoreFactory->get('eus_user_action')->get($this->currentUser()->id());
    if (!$accounts) {
      return $this->redirect('entity.user.collection');
    }

    $root = NULL;
    $names = [];
    $form['accounts'] = ['#tree' => TRUE];
    foreach ($accounts as $account) {
      $uid = $account->id();
      $names[$uid] = $account->label();
      // Prevent user 1 from being canceled.
      if ($uid <= 1) {
        $root = intval($uid) === 1 ? $account : $root;
        continue;
      }
      $form['accounts'][$uid] = [
        '#type' => 'hidden',
        '#value' => $uid,
      ];
    }

    $form['account']['names'] = [
      '#theme' => 'item_list',
      '#items' => $names,
    ];

    // Output a notice that user 1 cannot be canceled.
    if (isset($root)) {
      $redirect = (count($accounts) == 1);
      $message = $this->t('The user account %name session cannot be destroyed.', ['%name' => $root->label()]);
      $this->messenger()->addMessage($message, $redirect ? 'error' : 'warning');
      // If only user 1 was selected, redirect to the overview.
      if ($redirect) {
        return $this->redirect('entity.user.collection');
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current_user_id = $this->currentUser()->id();

    // Clear out the accounts from the temp store.
    $this->tempStoreFactory->get('eus_user_action')->delete($current_user_id);
    if ($form_state->getValue('confirm')) {
      foreach ($form_state->getValue('accounts') as $uid => $value) {
        // Prevent programmatic form submissions from destroying session of
        // user 1.
        if ($uid <= 1) {
          continue;
        }
        // Prevent user administrators from destroying themselves without
        // confirmation.
        if ($uid != $current_user_id) {
          $this->sessionManager->delete($uid);
        }
      }
    }

    $this->messenger()->addMessage($this->t('Session has been destroyed for all selected users.'));
    $form_state->setRedirect('entity.user.collection');
  }

}
