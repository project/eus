<?php

namespace Drupal\eus\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Driver\mysql\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmRoleEndSessionForm.
 */
class ConfirmRoleEndSessionForm extends ConfirmFormBase {

  /**
   * Drupal\Core\Session\SessionManagerInterface definition.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * ID of the item to end session.
   *
   * @var string
   */
  protected $role;

  /**
   * Use core services object.
   */
  public function __construct(SessionManagerInterface $session_manager, EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    $this->sessionManager = $session_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session_manager'),
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "confirm_eus_role_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $name = $this->entityTypeManager->getStorage('user_role')->load($this->role)->label();
    return $this->t('Are you sure you want to destroy all users session associated with %name role?', ['%name' => $name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user_role.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Destroy session');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $role = NULL) {
    $this->role = $role;
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    if (!array_key_exists($this->role, $roles)) {
      $this->messenger()->addError($this->t('%name is an invalid role.', ['%name' => $this->role]));
      return $this->redirect('entity.user_role.collection');
    }

    $users = $this->getUsersOfRole($this->role);
    if (empty($users)) {
      $this->messenger()->addError($this->t('No users associated with %name role.', ['%name' => $this->role]));
      return $this->redirect('entity.user_role.collection');
    }

    // Check if user 1 is there or not.
    if (in_array(1, $users)) {
      $account = $this->entityTypeManager->getStorage('user')->load(1);
      $this->messenger()->addWarning($this->t('The user account %name session cannot be destroyed.', ['%name' => $account->label()]));
      if (count($users) == 1) {
        return $this->redirect('entity.user_role.collection');
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current_user_id = $this->currentUser()->id();
    $users = $this->getUsersOfRole($this->role);
    foreach ($users as $uid) {
      // Prevent programmatic form submissions from destroying session of
      // user 1.
      if ($uid <= 1) {
        continue;
      }
      // Prevent user administrators from destroying themselves without
      // confirmation.
      if ($uid != $current_user_id) {
        $this->sessionManager->delete($uid);
      }
    }

    $this->messenger()->addMessage($this->t('Session has been destroyed for all users associated with %name role.', ['%name' => $this->role]));
    $form_state->setRedirect('entity.user_role.collection');
  }

  /**
   * Provide users associated with role.
   *
   * @param string $role
   *   String of role.
   *
   * @return nixed
   *   An array of user ids.
   */
  public function getUsersOfRole($role) {
    $query = $this->database->select('sessions', 'a');
    $query->fields('a', ['uid']);
    if ($role != 'authenticated') {
      $query->join('user__roles', 'b', 'a.uid = b.entity_id AND b.roles_target_id = :role', [':role' => $role]);
    }
    $ids = $query->execute()->fetchCol();
    return $ids;
  }

}
