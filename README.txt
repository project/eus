CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module allows admin users to destroy user session from interface.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/eus

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/eus


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Access end user session form (End User Session module)

     The top-level administration categories require this permission to be
     accessible. The 'End User Session' menu will be invisible from operations
     section unless this permission is granted.


MAINTAINERS
-----------

Current maintainers:
 * Ankush Gautam - https://www.drupal.org/u/ankushgautam76gmailcom
 * Ravi Kumar Singh (rksyravi) - https://www.drupal.org/u/rksyravi
