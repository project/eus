<?php

namespace Drupal\eus\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmEndSessionForm.
 */
class ConfirmEndSessionForm extends ConfirmFormBase {

  /**
   * Drupal\Core\Session\SessionManagerInterface definition.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ID of the item to end session.
   *
   * @var int
   */
  protected $user;

  /**
   * Use core services object.
   */
  public function __construct(SessionManagerInterface $session_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->sessionManager = $session_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "confirm_eus_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    if ($this->id == $this->currentUser()->id()) {
      return $this->t('Are you sure you want to destroy session of your account?');
    }
    $name = $this->entityTypeManager->getStorage('user')->load($this->id)->label();
    return $this->t('Are you sure you want to destroy session of account %name?', ['%name' => $name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Destroy session');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $user = NULL) {
    $this->id = $user;
    $account = $this->entityTypeManager->getStorage('user')->load($this->id);
    if (empty($account)) {
      throw new NotFoundHttpException();
    }

    // Redirect if user is admin.
    if ($this->id == 1) {
      $this->messenger()->addError($this->t('The user account %name session cannot be destroyed.', ['%name' => $account->label()]));
      return $this->redirect('entity.user.collection');
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->sessionManager->delete($this->id);
    $name = $this->entityTypeManager->getStorage('user')->load($this->id)->label();
    $this->messenger()->addMessage($this->t('%name session has been destroyed.', ['%name' => $name]));
    $form_state->setRedirect('entity.user.collection');
  }

}
